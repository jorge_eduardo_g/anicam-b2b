import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { CurrencyService } from '../../../../shared/services/currency.service';
import {LanguageService} from '../../../../shared/services/language.service';

@Component({
    selector: 'app-header-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
    languages = [
        {name: 'header.englishLabel', image: 'usa', code: 'en'},
        {name: 'header.spanishLabel', image: 'colombia', code: 'es'},
    ];

    currencies = [
        {name: '$USD (USA)', url: '', code: '$USD', symbol: '$'},
        {name: '$COP (Colombia)', url: '', code: '$COP', symbol: '$'},
    ];

    constructor(
        public currencyService: CurrencyService,
        public languageService: LanguageService,
        public translateService: TranslateService,
    ) {}

    ngOnInit(): void {
        console.log(this.translateService.instant('admin.processing'));
    }

    setCurrency(currency): void {
        this.currencyService.options = {
            code: currency.code,
            display: currency.symbol,
        };
    }

    setLanguage(language) {
        console.log(language);
        console.log(this.translateService.instant('admin.processing'));
        this.languageService.setLanguage(language);
    }
}
