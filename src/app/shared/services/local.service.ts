import { Injectable } from '@angular/core';
import {StorageService} from './storage.service';

@Injectable({
    providedIn: 'root'
})
export class LocalService {

    constructor(private storageService: StorageService) { }

    // Set the json data to local
    setJsonValue(key: string, value: any, encrypt= false) {
        if (encrypt) {
         return this.storageService.secureStorage.setItem(key, value);
        } else {
            return localStorage.setItem(key, JSON.stringify(value));
        }
    }

    // Get the json value from local
    getJsonValue(key: string, encrypt= false) {
        if (encrypt) {
         return this.storageService.secureStorage.getItem(key);
        } else {
            return JSON.parse(localStorage.getItem(key));
        }
    }

    // Remove the json value from local
    removeJsonValue(key: string, encrypt= false) {
        if (encrypt) {
         return this.storageService.secureStorage.removeItem(key);
        } else {
            return localStorage.removeItem(key);
        }
    }

    // Clear the local
    clearToken(encrypt= false) {
        if (encrypt) {
         return this.storageService.secureStorage.clear();
        } else {
            return localStorage.clear();
        }
    }

}
