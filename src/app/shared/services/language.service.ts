import {TranslateService} from '@ngx-translate/core';
import {Injectable} from '@angular/core';
import {LocalService} from './local.service';

@Injectable({
    providedIn: 'root'
})
export class LanguageService {

    selectedLanguage;

    constructor(
        private translate: TranslateService,
        private localService: LocalService,
    ) {
        let language = this.localService.getJsonValue('language');
        if (!language) {
            language = navigator.language.split('-')[0];
        }
        this.setLanguage(language);
    }

    setLanguage(language) {
        this.selectedLanguage = language;
        this.localService.setJsonValue('language', language);
        this.localService.setJsonValue('any', 'jorge');
        this.localService.setJsonValue('any', {data: language}, true);
        console.log(this.localService.getJsonValue('language'));
        console.log(this.localService.getJsonValue('any'));
        console.log(this.localService.getJsonValue('any', true));
        this.translate.use(language);
    }
}
